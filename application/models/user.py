from application import db

class User(db.Document):
	first_name = db.StringField()
	last_name = db.StringField()
	username = db.StringField()
	password = db.StringField()
	birthdate = db.DateTimeField()
	email = db.StringField()
	gender = db.StringField()
	phone = db.StringField()
	created_at = db.StringField()
