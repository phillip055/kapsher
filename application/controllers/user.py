from application import app
from flask import request,jsonify
#from application.utils.user import *
from application.models.user import *
from application.forms.user import *
from application.utils.date_time import *

@app.route("/kapsher/register",methods=['POST'])
def user_register():
    form = UserRegister()
    newUser = User()
    try:
        newUser.first_name = form.first_name.data
        newUser.last_name = form.last_name.data
        newUser.username = form.username.data
        newUser.password = form.password.data
        newUser.birthdate = timestamp_to_dt(form.birthdate.data)
        newUser.user_id = form.user_id.data
        newUser.email = form.email.data
        newUser.gender = form.gender.data
        newUser.phone = form.phone.data
        newUser.created_at = timestamp_to_dt(form.created_at.data)
        newUser.save()
        return jsonify(res=True)
    except:
        return jsonify(res=False)
