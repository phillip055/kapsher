import datetime
import time


def timestamp_to_dt(created_time):
    return datetime.datetime.utcfromtimestamp(int(created_time))
