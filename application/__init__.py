from flask import Flask,jsonify
from flask.ext.mongoengine import MongoEngine
app = Flask(__name__,static_folder='templates/static')
app.config.from_object('config')
db = MongoEngine(app)


from application.controllers.user import *
from application.controllers.frontend import *
