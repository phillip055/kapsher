from flask_wtf import Form
from wtforms import StringField


class UserRegister(Form):
    name = StringField('name')
    first_name = StringField("first_name")
    last_name = StringField("last_name")
    username = StringField("username")
    password = StringField("password")
    birthdate = StringField("birthdate")
    user_id = StringField("user_id")
    email = StringField("email")
    gender = StringField("gender")
    phone = StringField("phone")
    created_at = StringField("timestamp")
